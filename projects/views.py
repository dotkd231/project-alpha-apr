from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView
from projects.forms import CreateProject, CreateTask
from projects.models import Project
from django.contrib.auth.mixins import LoginRequiredMixin

from tasks.models import Task


class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "project_templates/list.html"
    context_object_name = "project_"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "project_templates/detail.html"
    context_object_name = "details"


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "project_templates/create.html"
    form_class = CreateProject

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.id])


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "task_templates/list.html"
    context_object_name = "task_"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    form_class = CreateTask
    template_name = "task_templates/create.html"

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.id])


class TaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    template_name = "task_templates/update.html"
    fields = ["is_completed"]
    success_url = reverse_lazy("show_my_tasks")
