from django.forms import ModelForm
from projects.models import Project
from tasks.models import Task


class CreateProject(ModelForm):
    class Meta:
        model = Project
        fields = ["name", "description", "members"]


class CreateTask(ModelForm):
    class Meta:
        model = Task
        fields = ["name", "start_date", "due_date", "project", "assignee"]
