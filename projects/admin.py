from django.contrib import admin
from projects.models import Project
from tasks.models import Task

admin.site.register(Project)
admin.site.register(Task)


class Task(admin.ModelAdmin):
    pass


class Project(admin.ModelAdmin):
    pass
